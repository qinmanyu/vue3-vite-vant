import { createRouter, createWebHashHistory } from "vue-router";

const router = createRouter({
  history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
  routes: [
    {
      path: "/",
      redirect: "/home",
    },
    {
      path: "/home",
      name: "home",
      component: () =>
        import(/* webpackChunkName: "home" */ "@/views/Home.vue"),
      meta: {
        index: 1,
      },
    },
    {
      path: "/category",
      name: "category",
      component: () =>
        import(/* webpackChunkName: "category" */ "@/views/Category.vue"),
      meta: {
        index: 1,
      },
    },
    {
      path: "/product-list",
      name: "product-list",
      component: () =>
        import(
          /* webpackChunkName: "product-list" */ "@/views/ProductList.vue"
        ),
      meta: {
        index: 2,
      },
    },
  ],
});

export default router;
