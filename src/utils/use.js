import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router/index'
// import filters from '@/filters'
// import directives from '@/directives'
import '@/style.css'

const app = createApp(App)
app.use(router)
// app.use(filters)
// app.use(directives)
app.mount('#app')
